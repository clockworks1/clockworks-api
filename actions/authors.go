package actions

import (
	"errors"
	"github.com/gobuffalo/buffalo"
	"github.com/gofrs/uuid"
	"clockworks_api/models"
)

// AuthorsList default implementation.
// @Summary Show authors
// @Tags authors
// @Accept  json
// @Produce  json
// @Success 200 {object} models.Author
// @Failure 404 {object} models.JsonError
// @Router /v1/authors [get]
func AuthorsList(c buffalo.Context) error {
	var authors models.Authors
	findErr := models.DB.All(&authors)

	if findErr != nil || len(authors) == 0 {
		return c.Render(404, r.JSON(models.JsonError{Error:404, Message:"No author found!"}))
	}

	return c.Render(200, r.JSON(authors))
}

// AuthorsShow default implementation.
// @Summary Show author
// @Tags authors
// @Accept  json
// @Produce  json
// @Success 200 {object} models.Author
// @Failure 404 {object} models.JsonError
// @Failure 500 {object} models.JsonError
// @Router /v1/auhors/{id} [get]
// @Param  id path string true "Author ID"
func AuthorsShow(c buffalo.Context) error {
	id, err := uuid.FromString(c.Param("id"))
	if err != nil {
		return c.Render(500, r.JSON(models.JsonError{Error:500, Message:"Not a valid uuid"}))
	}

	author := models.Author{}

	findErr := models.DB.Find(&author, id)
	if findErr != nil {
		return c.Render(404, r.JSON(models.JsonError{Error:404, Message:"Author not found!"}))
	}

	return c.Render(200, r.JSON(author))
}

// AuthorsCreate default implementation.
// @Summary Create author
// @Tags authors
// @Accept  json
// @Produce  json
// @Success 201 {object} models.Author
// @Failure 400 {object} models.JsonError
// @Failure 500 {object} models.JsonError
// @Router /v1/authors [post]
// @Param author body models.Author true "Add author"
func AuthorsCreate(c buffalo.Context) error {
	// new Author
	author := &models.Author{}

	if err := c.Bind(author); err != nil {
		return c.Error(400, err)
	}

	if err := validateAuthor(*author); err != nil {
		return c.Error(400, err)
	}

	newUuid, err := uuid.NewV4()
	if err != nil {
		return err
	}

	author.ID = newUuid

	// add in database
	_, err = models.DB.ValidateAndCreate(author)
	if err != nil {
		return c.Error(500, err)
	}

	return c.Render(201, r.JSON(author))
}

func validateAuthor(author models.Author) error {
	if author.Name == "" {
		return errors.New("author name cannot be empty");
	}
	return nil
}

