package actions

import (
	"encoding/json"
	"github.com/gobuffalo/buffalo"
	"github.com/gobuffalo/envy"
	forcessl "github.com/gobuffalo/mw-forcessl"
	paramlogger "github.com/gobuffalo/mw-paramlogger"
	"github.com/unrolled/secure"
	"gopkg.in/mail.v2"
	"net/http"
	"net/url"

	_ "clockworks_api/docs"
	"clockworks_api/models"

	"github.com/gobuffalo/buffalo-pop/pop/popmw"
	contenttype "github.com/gobuffalo/mw-contenttype"
	"github.com/gobuffalo/x/sessions"
	"github.com/rs/cors"
	buffaloSwagger "github.com/swaggo/buffalo-swagger"
	"github.com/swaggo/buffalo-swagger/swaggerFiles"
)

// ENV is used to help switch settings based on where the
// application is being run. Default is "development".
var ENV = envy.Get("GO_ENV", "development")
var app *buffalo.App

// App is where all routes and middleware for buffalo
// should be defined. This is the nerve center of your
// application.
//
// Routing, middleware, groups, etc... are declared TOP -> DOWN.
// This means if you add a middleware to `app` *after* declaring a
// group, that group will NOT have that new middleware. The same
// is true of resource declarations as well.
//
// It also means that routes are checked in the order they are declared.
// `ServeFiles` is a CATCH-ALL route, so it should always be
// placed last in the route declarations, as it will prevent routes
// declared after it to never be called.

// @title Clockworks API
// @version 1.0

// @contact.email simon@clockworks.fr

// @license.name Apache 2.0
// @license.url http://www.apache.org/licenses/LICENSE-2.0.html
func App() *buffalo.App {
	if app == nil {
		app = buffalo.New(buffalo.Options{
			Env:          ENV,
			SessionStore: sessions.Null{},
			PreWares: []buffalo.PreWare{
				cors.Default().Handler,
			},
			SessionName: "_clockworks_api_session",
		})

		// Automatically redirect to SSL (Apache & Certbot take care of this)
		// app.Use(forceSSL())

		// Log request parameters (filters apply).
		app.Use(paramlogger.ParameterLogger)

		// Set the request content type to JSON
		app.Use(contenttype.Set("application/json"))

		// Wraps each request in a transaction.
		//  c.Value("tx").(*pop.Connection)
		// Remove to disable this.
		app.Use(popmw.Transaction(models.DB))

		app.GET("/", HomeHandler)
		app.GET("/swagger/{doc:.*}", buffaloSwagger.WrapHandler(swaggerFiles.Handler))

		group := app.Group("/v1")

		group.GET("/authors", AuthorsList)
		group.GET("/authors/{id}", AuthorsShow)
		group.POST("/authors", AuthorsCreate)
		group.GET("/books", BooksList)
		group.GET("/books/{id}", BooksShow)
		group.POST("/books", BooksCreate)
		group.PUT("/books/{id}", BooksUpdate)
		group.DELETE("/books/{id}", BooksDelete)

		group.POST("/contact", Contact)
	}

	return app
}

func Contact(c buffalo.Context) error {
	//// Passed objects
	contact := &models.Contact{}

	if err := c.Bind(contact); err != nil {
		return c.Error(400, err)
	}

	_, err := checkRecaptcha(contact.Token)

	if err != nil {
		return c.Render(err.Error, r.JSON(err))
	}

	m := mail.NewMessage()
	m.SetHeader("From", contact.Email)
	m.SetHeader("To", "contact@clockworks.fr")
	m.SetHeader("Subject", contact.Subject)
	m.SetBody("text/html", "From " + contact.Name + "<br/>" + contact.Email + "<br/>" + contact.Message)

	d := mail.NewDialer("smtp.gmail.com", 465, envy.Get("SMTP_USER", ""), envy.Get("SMTP_PASS", ""))

	if err := d.DialAndSend(m); err != nil {
		return c.Render(500, r.JSON(&models.JsonError{Error: 500, Message: "cannot send email"}))
	}

	return c.Render(204, nil)
}

func checkRecaptcha(token string) (interface{}, *models.JsonError) {
	body := url.Values{
		"secret":   {envy.Get("RECAPTCHA_PRIVATE_KEY", "")},
		"response": {token},
	}

	response, err := http.PostForm("https://www.google.com/recaptcha/api/siteverify", body)

	if err != nil {
		return nil, &models.JsonError{Error: 500, Message: "client error"}
	}

	var mapResponse map[string]interface{}

	json.NewDecoder(response.Body).Decode(&mapResponse)

	if mapResponse["success"] == false {
		return nil, &models.JsonError{Error: 403, Message: "invalid captcha"}
	}


	return mapResponse["success"], nil
}

// forceSSL will return a middleware that will redirect an incoming request
// if it is not HTTPS. "http://example.com" => "https://example.com".
// This middleware does **not** enable SSL. for your application. To do that
// we recommend using a proxy: https://gobuffalo.io/en/docs/proxy
// for more information: https://github.com/unrolled/secure/
func forceSSL() buffalo.MiddlewareFunc {
	return forcessl.Middleware(secure.Options{
		SSLRedirect:     ENV == "production",
		SSLProxyHeaders: map[string]string{"X-Forwarded-Proto": "https"},
	})
}
