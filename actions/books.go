package actions

import (
	"errors"
	"github.com/gobuffalo/buffalo"
	"github.com/gofrs/uuid"
	"clockworks_api/models"
)

// BooksList default implementation.
// @Summary Show books
// @Tags books
// @Accept  json
// @Produce  json
// @Success 200 {object} models.Book
// @Failure 404 {object} models.JsonError
// @Router /v1/books [get]
func BooksList(c buffalo.Context) error {
	var books models.Books
	findErr := models.DB.All(&books)

	if findErr != nil || len(books) == 0 {
		return c.Render(404, r.JSON(models.JsonError{Error: 404, Message: "No books found!"}))
	}

	return c.Render(200, r.JSON(books))
}

// BooksShow default implementation.
// @Summary Show book
// @Tags books
// @Accept  json
// @Produce  json
// @Success 200 {object} models.Book
// @Failure 404 {object} models.JsonError
// @Failure 500 {object} models.JsonError
// @Router /v1/books/{id} [get]
// @Param  id path string true "Book ID"
func BooksShow(c buffalo.Context) error {
	id, err := uuid.FromString(c.Param("id"))
	if err != nil {
		return c.Render(500, r.JSON(models.JsonError{Error: 500, Message: "Not a valid uuid"}))
	}

	book := models.Book{}

	findErr := models.DB.Eager("Author").Find(&book, id)
	if findErr != nil {
		return c.Render(404, r.JSON(models.JsonError{Error: 404, Message: "Book not found!"}))
	}

	return c.Render(200, r.JSON(book))
}

// BooksCreate default implementation.
// @Summary Create book
// @Tags books
// @Accept  json
// @Produce  json
// @Success 201 {object} models.Book
// @Failure 400 {object} models.JsonError
// @Failure 500 {object} models.JsonError
// @Router /v1/books [post]
// @Param book body models.Book true "Add book"
func BooksCreate(c buffalo.Context) error {
	// new Book
	book := &models.Book{}

	if err := c.Bind(book); err != nil {
		return c.Error(400, err)
	}

	if err := validateBook(*book); err != nil {
		return c.Error(400, err)
	}

	newUuid, err := uuid.NewV4()
	if err != nil {
		return err
	}

	book.ID = newUuid

	// add in database
	_, err = models.DB.ValidateAndCreate(book)
	if err != nil {
		return c.Error(500, err)
	}

	return c.Render(201, r.JSON(book))
}

// @Summary Delete book
// @Tags books
// @Accept  json
// @Produce  json
// @Success 204 {object} string
// @Failure 404 {object} models.JsonError
// @Failure 400 {object} models.JsonError
// @Failure 500 {object} models.JsonError
// @Router /v1/books/{id} [delete]
// @Param  id path string true "Book ID"
func BooksDelete(c buffalo.Context) error {
	id, err := uuid.FromString(c.Param("id"))

	if err != nil {
		return c.Render(500, r.JSON(models.JsonError{Error: 500, Message: "Not a valid uuid"}))
	}

	book := models.Book{}

	findErr := models.DB.Find(&book, id)
	if findErr != nil {
		return c.Render(404, r.JSON(models.JsonError{Error: 404, Message: "Book not found!"}))
	}

	delErr := models.DB.Destroy(&book)
	if delErr != nil {
		return c.Render(400, r.JSON(models.JsonError{Error: 400, Message: "Unable to delete book"}))
	}

	return c.Render(204, nil)
}

// @Summary Update book
// @Tags books
// @Accept  json
// @Produce  json
// @Success 201 {object} models.Book
// @Failure 400 {object} models.JsonError
// @Failure 500 {object} models.JsonError
// @Router /v1/books [put]
// @Param book body models.Book true "Update book"
func BooksUpdate(c buffalo.Context) error {
	// new Book
	newBook := &models.Book{}

	if err := c.Bind(newBook); err != nil {
		return c.Error(400, err)
	}

	// Control if data is ok to be persisted
	if err := validateBook(*newBook); err != nil {
		return c.Render(400, r.JSON(models.JsonError{Error: 400, Message: err.Error()}))
	}

	id, err := uuid.FromString(c.Param("id"))

	if err != nil {
		return c.Render(500, r.JSON(models.JsonError{Error: 500, Message: "Not a valid uuid"}))
	}

	book := models.Book{}
	findErr := models.DB.Find(&book, id)

	if findErr != nil {
		return c.Render(404, r.JSON(models.JsonError{Error: 404, Message: "Book not found!"}))
	}

	book.Name = newBook.Name
	book.AuthorID = newBook.AuthorID

	_, updateErr := models.DB.ValidateAndUpdate(&book)
	if updateErr != nil {
		return errors.New("error while updating a book")
	}

	return c.Render(200, r.JSON(book))
}


func validateBook(book models.Book) error {
	nullUuid:= uuid.NullUUID{}

	switch {
	case book.AuthorID == nullUuid.UUID:
		return errors.New("author id is missing")
	case book.Name == "":
		return errors.New("name is missing")
	}

	return nil
}