# Welcome to Clockworks API!

Thank you for choosing Buffalo for your web development needs, and my seed version embedding Docker Compose.

## Usage

Use the .env.dist file to make a .env file with environment variables suiting your needs

Check .docker/start.sh file and uncomment the line for first database creation

Use `swag init -g actions/app.go` to generate Swagger documentation.

Start local development server with `docker-compose up`

Enjoy!

[Powered by Buffalo](http://gobuffalo.io)
