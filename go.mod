module clockworks_api

go 1.13

require (
	github.com/Masterminds/semver/v3 v3.1.0 // indirect
	github.com/alecthomas/template v0.0.0-20190718012654-fb15b899a751
	github.com/cockroachdb/cockroach-go v0.0.0-20200411195601-6f5842749cfc // indirect
	github.com/fsnotify/fsnotify v1.4.9 // indirect
	github.com/go-openapi/spec v0.19.7 // indirect
	github.com/go-openapi/swag v0.19.9 // indirect
	github.com/gobuffalo/buffalo v0.16.5
	github.com/gobuffalo/buffalo-pop v1.23.1
	github.com/gobuffalo/envy v1.9.0
	github.com/gobuffalo/mw-contenttype v0.0.0-20190129203934-2554e742333b
	github.com/gobuffalo/mw-forcessl v0.0.0-20180802152810-73921ae7a130
	github.com/gobuffalo/mw-paramlogger v0.0.0-20190129202837-395da1998525
	github.com/gobuffalo/nulls v0.4.0 // indirect
	github.com/gobuffalo/packr/v2 v2.8.0
	github.com/gobuffalo/pop v4.13.1+incompatible
	github.com/gobuffalo/suite v2.8.2+incompatible
	github.com/gobuffalo/validate v2.0.4+incompatible
	github.com/gobuffalo/validate/v3 v3.3.0 // indirect
	github.com/gobuffalo/x v0.0.0-20190224155809-6bb134105960
	github.com/gofrs/uuid v3.2.0+incompatible
	github.com/karrick/godirwalk v1.15.6 // indirect
	github.com/mailru/easyjson v0.7.1 // indirect
	github.com/markbates/grift v1.5.0
	github.com/mattn/go-colorable v0.1.6 // indirect
	github.com/rs/cors v1.7.0
	github.com/spf13/cobra v1.0.0 // indirect
	github.com/swaggo/buffalo-swagger v0.0.0-20181212061503-01d66cd14ca7
	github.com/swaggo/swag v1.6.5
	github.com/unrolled/secure v0.0.0-20190103195806-76e6d4e9b90c
	golang.org/x/crypto v0.0.0-20200420104511-884d27f42877 // indirect
	golang.org/x/net v0.0.0-20200324143707-d3edc9973b7e // indirect
	golang.org/x/sys v0.0.0-20200420163511-1957bb5e6d1f // indirect
	golang.org/x/tools v0.0.0-20200420001825-978e26b7c37c // indirect
	gopkg.in/mail.v2 v2.0.0-20180731213649-a0242b2233b4
)
