#!/bin/bash

swag init -g actions/app.go
# Uncomment for first database creation
# buffalo pop create
buffalo pop migrate
buffalo dev